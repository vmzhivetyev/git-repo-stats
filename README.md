# git-repo-stats

## What it does?

Shows impact of every contributor who ever committed to the repo.

## Usage

Move `stats.py` to the root of your repo.
```
python3 stats.py
```

## Example

##### Part of output for `RxSwift` repo

```
Scanning...
Sorted stats:
Krunoslav Zaher <krunoslav.zaher@gmail.com>:            +169702     -130001     =39701     
Krunoslav Zaher <kzaher@Krunoslavs-MacBook-Pro.local>:  +59906      -16852      =43054     
freak4pc <freak4pc@gmail.com>:                          +9322       -8742       =580       
beeth0ven <beeth0vendev@163.com>:                       +7909       -7774       =135       
Jamie Pinkham <jamie.pinkham@snagajob.com>:             +4360       -4256       =104       
yury <yury.korolev@gmail.com>:                          +5971       -2101       =3870      
sergdort <sergdort@gmail.com>:                          +5256       -1798       =3458      
Carlos García <carlosypunto@gmail.com>:                 +4011       -2044       =1967      
Czajnikowski <czajnikowski@gmail.com>:                  +2101       -1596       =505       
Scott Gardner <scott.gardner@mac.com>:                  +1453       -1728       =-275      
Michael Long <mlong@clientresourcesinc.com>:            +1767       -1350       =417       
Mo Ramezanpoor <me@mohsenr.com>:                        +1613       -1464       =149       
Junior B <junior@bonto.ch>:                             +2720       -167        =2553      
Carlos García <rallapunto@hotmail.com>:                 +2281       -592        =1689      
tarunon <croissant9603@gmail.com>:                      +1779       -1070       =709       
Tomi Koskinen <tomi.koskinen@reaktor.fi>:               +2563       -222        =2341
```
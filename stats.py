#!/usr/local/bin/python3

import os
import sys
import subprocess
import re

def ignore_exception(IgnoreException=Exception,DefaultVal=None):
    """ Decorator for ignoring exception from a function
    e.g.   @ignore_exception(DivideByZero)
    e.g.2. ignore_exception(DivideByZero)(Divide)(2/0)
    """
    def dec(function):
        def _dec(*args, **kwargs):
            try:
                return function(*args, **kwargs)
            except IgnoreException:
                return DefaultVal
        return _dec
    return dec

def get_numbers(email):
	cmd = f"git log --author={email} --pretty=tformat: --numstat | awk '{{ add += $1; subs += $2; loc += $1 - $2 }} END {{ printf \"%s %s %s\", add, subs, loc }}' -"
	output = subprocess.getoutput(cmd)

	sint = ignore_exception(ValueError)(int)
	return {
		"added": sint(output.split(' ')[0]) or 0,
		"removed": sint(output.split(' ')[1]) or 0,
		"result": sint(output.split(' ')[2]) or 0
	}

def get_contributors():
    output = subprocess.getoutput("git shortlog -sne --all")

    result = {}
    for line in re.sub(r"^\s+\d+\s+", "", output, flags=re.MULTILINE).split('\n'):
    	name = line.split(' <')[0]
    	email = line.split(' <')[1][:-1]

    	if email not in result:
	    	result[email] = {
	    		"name":name
	    	}

    return result

def line_of_length(line, length):
	return line + (' '*(length - len(line)))

def scan(members):
	for email, val in members.items():
		name = line_of_length(f"{val['name']} <{email}>: ", 50)
		# print(name, end='', flush=True)

		val["stats"] = get_numbers(email)

		added = line_of_length(str(val["stats"]["added"]), 10)
		removed = line_of_length(str(val["stats"]["removed"]), 10)
		result = line_of_length(str(val["stats"]["result"]), 10)

		# print(f'+{added} -{removed} ={result}')

def psort(members):
	pairs = sorted(members.items(), key=lambda kv: -1*(kv[1]["stats"]["added"] + kv[1]["stats"]["removed"]))

	for email, val in pairs:
		name = line_of_length(f"{val['name']} <{email}>: ", 50)
		print(name, end='', flush=True)

		added = line_of_length(str(val["stats"]["added"]), 10)
		removed = line_of_length(str(val["stats"]["removed"]), 10)
		result = line_of_length(str(val["stats"]["result"]), 10)

		print(f'+{added} -{removed} ={result}')

print("Scanning...")
contribs = get_contributors()
scan(contribs)
print("Sorted stats:")
psort(contribs)


